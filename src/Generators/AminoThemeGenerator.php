<?php

namespace Drupal\amino_tools\Generators;

use DrupalCodeGenerator\Command\BaseGenerator;
use DrupalCodeGenerator\Utils;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Question\ConfirmationQuestion;

/**
 * Generates a subtheme of Amino.
 */
class AminoThemeGenerator extends BaseGenerator {

  /**
   * {@inheritdoc}
   */
  protected $name = 'amino';

  /**
   * {@inheritdoc}
   */
  protected $description = 'Generates a subtheme of Amino';

  /**
   * {@inheritdoc}
   */
  protected $templatePath = __DIR__ . '/../../templates';

  /**
   * {@inheritdoc}
   */
  protected $destination = '';

  /**
   * {@inheritdoc}
   */
  protected function interact(InputInterface $input, OutputInterface $output) {
    $name_question['name'] = new Question('Theme name', 'Amino subtheme');
    $this->collectVars($input, $output, $name_question);

    $questions['machine'] = new Question('Machine name', Utils::human2machine($this->vars['name']));
    $questions['description'] = new Question('Description', 'Custom subtheme based on the Amino theme.');
    $questions['theme_path'] = new Question('Theme path', 'themes/custom');
    $questions['layout'] = new Question('Layout system (flexbox or grid)', 'flexbox');
    $questions['script'] = new ConfirmationQuestion('Include a javascript template?', TRUE);

    $this->collectVars($input, $output, $questions);
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $output->writeln('Generating files...');

    $theme_path = $this->vars['theme_path'] . '/' . $this->vars['machine'] . '/';

    $this->addFile()
      ->path($theme_path . '{machine}.info.yml')
      ->template('info.twig');

    $this->addFile()
      ->path($theme_path . '{machine}.libraries.yml')
      ->template('libraries.twig');

    $this->addFile()
      ->path($theme_path . '{machine}.breakpoints.yml')
      ->template('breakpoints.twig');

    $this->addFile()
      ->path($theme_path . '{machine}.theme')
      ->template('theme.twig');

    $this->addFile()
      ->path($theme_path . 'package.json')
      ->template('package.twig');

    $this->addFile()
      ->path($theme_path . '.gitignore')
      ->template('gitignore.twig');

    $this->addFile()
      ->path($theme_path . 'gulpfile.js')
      ->template('gulpfile.twig');

    $this->addFile()
      ->path($theme_path . 'config.js')
      ->template('config.twig');

    $this->addFile()
      ->path($theme_path . 'color/color.inc')
      ->template('color/color.twig');

    $this->addFile()
      ->path($theme_path . 'color/preview.css')
      ->template('color/preview.css.twig');

    $this->addFile()
      ->path($theme_path . 'color/preview.html')
      ->template('color/preview.html.twig');

    $this->addFile()
      ->path($theme_path . 'color/preview.js')
      ->template('color/preview.js.twig');

    $this->addFile()
      ->path($theme_path . 'css/color.css')
      ->template('css/color.css.twig');

    $scss_files = [
      'styles.scss',
      '_config.scss',
      'base/_fonts.scss',
      'base/_global.scss',
      'base/_mixins.scss',
      'components/_breadcrumb.scss',
      'components/_buttons.scss',
      'components/_dialog.scss',
      'components/_elements.scss',
      'components/_forms.scss',
      'components/_layout.scss',
      'components/_messages.scss',
      'components/_node-edit.scss',
      'components/_tabs.scss',
      'layout/_flex.scss',
      'layout/_grid.scss',
      'pages/_user-auth.scss',
    ];

    foreach ($scss_files as $file) {
      $this->addFile()
        ->path($theme_path . 'scss/' . $file)
        ->template('scss/' . $file . '.twig');
    }

    if ($this->vars['script']) {
      $this->addFile()
        ->path($theme_path . 'js/source/{machine}.js')
        ->template('script.twig');
    }

    parent::execute($input, $output);
  }

}
